<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
	<title>注册</title>
	<link rel="stylesheet" href="css/index.css">
	<script src="js/register.js"></script>
</head>
<body>
	<div class="top gaintro_top">
		<div class="center_son topson  gaintro_topson">
			<a class="logo" href="">
				<img src="img/logo.png">
			</a>
			<div class="head_fa">
				<ul class="head_son">
					<li class="merchant_fa">
						<a class="heada" href="index.jsp">商店
						</a>					</li>
					<li class="community_fa">
						<a class="heada" href="shoppingcar.jsp">购物车
						</a>
					</li>
					<li>
						<a class="heada" href="">关于</a>
					</li>
					<li>
						<a class="heada" href="">客服</a>
					</li>
				</ul>
			</div>
			<div class="login">
					<!-- <a class="log1" href="login.html">登陆</a> -->
					<c:if test="${not empty user }">
						<a href="${pageContext.request.contextPath }/LoginoutServlet"><h6 style="color: white;">欢迎你回来${sessionScope.user.username }!</h6>注销</a>
					<span>|</span>
						<a href="${pageContext.request.contextPath }/signup.jsp">注册</a>
					</c:if>
					
					<c:if test="${empty user }">
						<a href="${pageContext.request.contextPath }/loginjsp.jsp">登录</a>
						<span>|</span>
						<a href="${pageContext.request.contextPath }/signup.jsp">注册</a>
					</c:if>
					<!-- <a class="log1" href="signup.html">注册</a> -->
				</div>
		</div>
	</div>
	<form action="validation.do" method="post">
	<div class="bg">
		<div class="center Fatherbox gaintro_father">
			<div class="center_son signup">
				<div class="logL_fa">
					<div class="logLeft">
						<div class="logLeft_son1">
							<h2>创建您的账户</h2>
							<font size="4" color=red >
							<div id="results">请输入用户名和密码</div></font><br>
							<div class="logLeft_son">
								<p class="logLp">Steam 账户名称</p>
								<div>
									<input type="text" name="username" id="username" onblur="validate()">
								</div>
							</div>
							<div class="logLeft_son">
								<p class="logLp">Steam 密码</p>
								<div>
									<input type="password" name="password" id="pass">
								</div>
							</div>
							<div class="logLeft_son">
								<label for="vcode">验证码：</label>
								<input type="text" name="verifycode" class="form-control" id="verifycode" placeholder="请输入验证码" style="width: 120px;"/>
								<br>${sessionScope.login_message}
								<div style="margin-left:45px;">
								<a href="javascript:refreshCode();">
									<img src="${pageContext.request.contextPath}/CheckCodeServlet" title="看不清点击刷新" id="vcode"/>
								</a>
								</div>
							</div>
							<p style="margin:10px 0px 20px 0px;">
								<input type="checkbox" id="time" style="outline:none;position:relative;top:2px;margin-right:5px">
								<label for="time">同意Steam订阅者协议和Valve隐私政策的条款。</label>
							</p>
						</div>
						<div class="btn_fa">
							<div class="logBtnL">
								 <a href="transition.jsp"><button class="logBtn">注册账户</button></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</form>
	<div class="tablist">
		<div class="center copyright gaintro_bot">
			<div class="center_son bottomlog gintro_bottom">
				<div>
					<a href="" class="logimg">
						<img src="img/logo_valve_footer.png">
					</a>
					<div class="botlogText">
						<p>©2018 Valve Corporation。保留所有权利。所有商标均为其在美国及其它国家/地区的各自持有者所有。</p>
						<p>
								所有的价格均已包含增值税（如适用）。
							<a href="">隐私政策</a> &nbsp;|&nbsp; 
							<a href="">法律信息</a> &nbsp;|&nbsp; 
							<a href="">Steam 订户协议</a> &nbsp;|&nbsp; 
							<a href="">退款	</a>
						</p>
					</div>
					<a class="logimg1" href=""><img src="img/logo_steam_footer.png"></a>
				</div>
				<div class="youlian">
					<a href="">关于 Valve</a> &nbsp;| &nbsp; 
					<a href="">Steamworks</a> &nbsp;|&nbsp; 
					<a href="">工作</a> &nbsp;|&nbsp; 
					<a href="">Steam 分销</a> &nbsp;|&nbsp; 
					<a href="">礼物卡</a> &nbsp;|&nbsp; 
					<a class="facebook" href="">Steam</a> &nbsp;|&nbsp; 
					<a class="twiter" href="">@steam_games</a>
				</div>
			</div>
		</div>
	</div>
	<script src="js/best.js"></script>
	<script src="js/jquery.js"></script>
	<script src="js/index.js"></script>

</body>
</html>