<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
     <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
     <%@ page import="java.util.ArrayList,com.steam.bean.GamesItem" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>修改产品信息</title>
		<link rel="stylesheet" href="css2/index.css">
	</head>
<body>

<%
GamesItem p = (GamesItem)request.getAttribute("m");
%>
	<div class="top gaintro_top">
		<div class="center_son topson  gaintro_topson">
			<a class="logo" href="">
				<img src="img/logo.png">
			</a>
			<div class="head_fa">
				<ul class="head_son">
					<li class="merchant_fa">
						<a class="heada" href="index.html">Steam后台管理系统
						</a>
					</li>
				</ul>
			</div>
		<div class="login">
				<a class="log1" href="login.html">登陆</a>
			</div>
		</div>
	</div>
	<div class="bg">
		<div class="center Fatherbox gaintro_father">
			<div class="center_son signup ">
				<div class="logL_fa">
					<div class="product">
						<div class="logLeft_son1">
							<form action="adminUpdateSecondServlet" method="post" > 
								<table width="450px"> 
								  <tr> 
									<td colspan="2"> 
									  <h2>修改产品信息</h2> 
									</td> 
								  </tr> 
								<tr> 
									<td >游戏名：</td> 
									<td><input type="text" name="gamename" value="<%=p.getGamename()%>"></td> 
								  </tr> 
								  <tr> 
									<td>游戏价格：</td> 
									<td><input type="text" name="price" value="<%=p.getPrice()%>"></td> 
								  </tr> 
								  <tr> 
									<td>游戏简介：</td> 
									<td><input type="text" name="introduce" value="<%=p.getIntroduce()%>"/></td> 
								  </tr> 
								  <tr> 
									<td colspan="2">
										<button class="changeBtn" type="submit">修改</button>
									</td> 
								  </tr> 
								</table> 
							  </form> 						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="tablist">
		<div class="center copyright gaintro_bot">
			<div class="center_son bottomlog gintro_bottom">
				<div>
					<a href="" class="logimg">
						<img src="img/logo_valve_footer.png">
					</a>
					<div class="botlogText">
						<p>©2018 Valve Corporation。保留所有权利。所有商标均为其在美国及其它国家/地区的各自持有者所有。</p>
						<p>
								所有的价格均已包含增值税（如适用）。
							<a href="">隐私政策</a> &nbsp;|&nbsp; 
							<a href="">法律信息</a> &nbsp;|&nbsp; 
							<a href="">Steam 订户协议</a> &nbsp;|&nbsp; 
							<a href="">退款	</a>
						</p>
					</div>
					<a class="logimg1" href=""><img src="img/logo_steam_footer.png"></a>
				</div>
				<div class="youlian">
					<a href="">关于 Valve</a> &nbsp;| &nbsp; 
					<a href="">Steamworks</a> &nbsp;|&nbsp; 
					<a href="">工作</a> &nbsp;|&nbsp; 
					<a href="">Steam 分销</a> &nbsp;|&nbsp; 
					<a href="">礼物卡</a> &nbsp;|&nbsp; 
					<a class="facebook" href="">Steam</a> &nbsp;|&nbsp; 
					<a class="twiter" href="">@steam_games</a>
				</div>
			</div>
		</div>
	</div>
	<script src="js/best.js"></script>
	<script src="js/jquery.js"></script>
	<script src="js/index.js"></script>
</body>
</html>