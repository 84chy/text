var xmlHttp;
function createXMLHttpRequest(){
	if(window.ActiveXObject){
		xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
	}else if(window.XMLHttpRequest){
		xmlHttp=new XMLHttpRequest();
	}
}

function validate(){
	createXMLHttpRequest();
	var username=document.getElementById("username");
	var url="validation.do?username="+escape(username.value);
	xmlHttp.open("GET",url,true);
	xmlHttp.onreadystatechange=handleStateChange;
	xmlHttp.send(null);
	}
	
	function handleStateChange(){
		if(xmlHttp.readyState==4){
			if(xmlHttp.status==200){
				var message=xmlHttp.responseXML.getElementsByTagName("message")[0].firstChild.data;
				var messageArea=document.getElementById("results");
				messageArea.innerHTML="<p>"+message+"</p>";
			}
		}
	}