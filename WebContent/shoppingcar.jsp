<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>	
<!DOCTYPE html>
<html>
<head>
	<title>购物车</title>
	<link rel="stylesheet" href="css/index.css">
</head>
<body>
	<div class="top gaintro_top">
		<div class="center_son topson  gaintro_topson">
			<a class="logo" href="">
				<img src="img/logo.png">
			</a>
			<div class="head_fa">
				<ul class="head_son">
					<li class="merchant_fa">
						<a class="heada" href="index.jsp">商店
						</a>					</li>
					<li class="community_fa">
						<a class="heada" href="shoppingcar.jsp">购物车
						</a>
					</li>
					<li>
						<a class="heada" href="">关于</a>
					</li>
					<li>
						<a class="heada" href="">客服</a>
					</li>
				</ul>
			</div>
		<div class="login">
					<!-- <a class="log1" href="login.html">登陆</a> -->
					<c:if test="${not empty user }">
						<a href="${pageContext.request.contextPath }/LoginoutServlet"><h6 style="color: white;">欢迎你回来${sessionScope.user.username }!</h6>注销</a>
					<span>|</span>
						<a href="${pageContext.request.contextPath }/signup.jsp">注册</a>
					</c:if>
					
					<c:if test="${empty user }">
						<a href="${pageContext.request.contextPath }/loginjsp.jsp">登录</a>
						<span>|</span>
						<a href="${pageContext.request.contextPath }/signup.jsp">注册</a>
					</c:if>
					<!-- <a class="log1" href="signup.html">注册</a> -->
				</div>
		</div>
	</div>
	<div class="bg">
		<div class="center Fatherbox gaintro_father">
			<div class="center_son signup">
				<div class="logL_fa">
					<div class="shoppingcontent">
							<h2>你的购物车</h2>
                            <div class="game1">
                                <a href=""><img src="img/game1.jpg"></a>
                                <div class="gameText">
                                    <p class="gamep1">PLAYERUNKNOWN'S BATTLEGROUNDS</p>
                                    <div class="gamediv1">
                                        <span></span>
                                    </div>
                                    <span>￥99</span>
                                </div>   
                            </div>
                            <!-- 列表增加 -->
                        </div>
                        <a href="finish.jsp"><button class="green tianjia1shoppingcar">确认购买</button></a>
					</div>
				</div>
			</div>
		</div>
	<div class="bg">
		<div class="center Fatherbox gaintro_father">
			<div class="center_son signup">
				<div class="logL_fa">
			       
				</div>
			</div>
		</div>
		</div>
	<div class="tablist">
		<div class="center copyright gaintro_bot">
			<div class="center_son bottomlog gintro_bottom">
				<div>
					<a href="" class="logimg">
						<img src="img/logo_valve_footer.png">
					</a>
					<div class="botlogText">
						<p>©2018 Valve Corporation。保留所有权利。所有商标均为其在美国及其它国家/地区的各自持有者所有。</p>
						<p>
								所有的价格均已包含增值税（如适用）。
							<a href="">隐私政策</a> &nbsp;|&nbsp; 
							<a href="">法律信息</a> &nbsp;|&nbsp; 
							<a href="">Steam 订户协议</a> &nbsp;|&nbsp; 
							<a href="">退款	</a>
						</p>
					</div>
					<a class="logimg1" href=""><img src="img/logo_steam_footer.png"></a>
				</div>
				<div class="youlian">
					<a href="">关于 Valve</a> &nbsp;| &nbsp; 
					<a href="">Steamworks</a> &nbsp;|&nbsp; 
					<a href="">工作</a> &nbsp;|&nbsp; 
					<a href="">Steam 分销</a> &nbsp;|&nbsp; 
					<a href="">礼物卡</a> &nbsp;|&nbsp; 
					<a class="facebook" href="">Steam</a> &nbsp;|&nbsp; 
					<a class="twiter" href="">@steam_games</a>
				</div>
			</div>
		</div>
	</div>
	<script src="js/best.js"></script>
	<script src="js/jquery.js"></script>
	<script src="js/index.js"></script>
</body>
</html>