<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>登陆</title>
<link rel="stylesheet" href="css/index.css">
</head>
<body>
<%--如果用户已登录,直接掉转到首页 --%>
      <c:if test="${not empty user }">
          <jsp:forward page="/index.jsp"></jsp:forward>
      </c:if>
	<div class="top gaintro_top">
		<div class="center_son topson  gaintro_topson">
			<a class="logo" href="">
				<img src="img/logo.png">
			</a>
			<div class="head_fa">
				<ul class="head_son">
					<li class="merchant_fa">
						<a class="heada" href="index.jsp">商店
						</a>					</li>
					<li class="community_fa">
						<a class="heada" href="shoppingcar.jsp">购物车
						</a>
					</li>
					<li>
						<a class="heada" href="">关于</a>
					</li>
					<li>
						<a class="heada" href="">客服</a>
					</li>
				</ul>
			</div>
			<div class="login">
					<!-- <a class="log1" href="login.html">登陆</a> -->
					<c:if test="${not empty user }">
						<a href="${pageContext.request.contextPath }/LoginoutServlet"><h6 style="color: white;">欢迎你回来${sessionScope.user.username }!</h6>注销</a>
					<span>|</span>
						<a href="${pageContext.request.contextPath }/signup.jsp">注册</a>
					</c:if>
					
					<c:if test="${empty user }">
						<a href="${pageContext.request.contextPath }/loginjsp.jsp">登录</a>
						<span>|</span>
						<a href="${pageContext.request.contextPath }/signup.jsp">注册</a>
					</c:if>
					<!-- <a class="log1" href="signup.html">注册</a> -->
				</div>
		</div>
	</div>
	<div class="bg">
		<div class="center Fatherbox gaintro_father">
			<div class="center_son logIn">
				<div class="logL_fa">
					<div class="logLeft">
					<form action="${pageContext.request.contextPath }/LoginCheckServlet" method="post">
						<div class="logLeft_son1">
							<h2>登陆</h2>
							<p class="logP1">到现有的 Steam 账户</p>
							<div class="logLeft_son">
							${sessionScope.message }<br>
								<p class="logLp">Steam 账户名称</p>
								<div>
									<input type="text" id="username" name="username">
								</div>
							</div>
							<div class="logLeft_son">
								<p class="logLp">Steam 密码</p>
								<div>
									<input type="password" id="pass" name="password">
								</div>
							</div>
							<div class="logLeft_son">
								<label for="vcode">验证码：</label>
								<input type="text" name="verifycode" class="form-control" id="verifycode" placeholder="请输入验证码" style="width: 120px;"/>
								<br>${sessionScope.login_message}
								<div style="margin-left:45px;">
								<a href="javascript:refreshCode();">
									<img src="${pageContext.request.contextPath}/CheckCodeServlet" title="看不清点击刷新" id="vcode"/>
								</a>
								</div>
							</div>
							<p style="margin:10px 0px 20px 0px;">
								<input type="checkbox" name="check" value="check" id="time" style="outline:none;position:relative;top:2px;margin-right:5px" >
								<label for="time">7天内免登陆</label>
							</p>
							
						</div>
						<div class="logLeft_son2">
							
						</div>
						<div class="logLeft_son3">
							<h2>创建</h2>
							<p class="logP1">一个新的免费帐户</p>
							<p>欢迎免费加入及轻松使用。继续创建 Steam 帐户并获取 Steam - 适合 PC 和 Mac 玩家的前沿数字解决方案。</p>
						</div>
						<div class="btn_fa">
							<div class="logBtnL">
								<button class="logBtn" type="submit">登陆</button>				
							</div>
							<div class="logBtnR">
								 <a href="signup.jsp"><button class="logBtn">加入 steam</button></a>
							</div>
						</div>
						</form>
					</div>
					<div class="loga"><a href="">忘记您的密码？</a></div>
				</div>
				<div class="logR_fa">
					<div class="logRight">
						<h2>为什么加入 STEAM</h2>
						<ul>
							<li>购买和下载完整零售游戏</li>
							<li>加入 Steam 社区</li>
							<li>游戏时与好友聊天</li>
							<li>在任何电脑上都能玩</li>
							<li>安排游戏、比赛或 LAN 聚会</li>
							<li>获取自动游戏更新以及更多！</li>
						</ul>
						<img src="img/why_join_preview.png">
					</div>
					<div class="loga"><a href="">了解更多 Steam 的相关信息</a></div>
				</div>
			</div>
		</div>
	</div>
	<div class="tablist">
		<div class="center copyright gaintro_bot">
			<div class="center_son bottomlog gintro_bottom">
				<div>
					<a href="" class="logimg">
						<img src="img/logo_valve_footer.png">
					</a>
					<div class="botlogText">
						<p>©2018 Valve Corporation。保留所有权利。所有商标均为其在美国及其它国家/地区的各自持有者所有。</p>
						<p>
								所有的价格均已包含增值税（如适用）。
							<a href="">隐私政策</a> &nbsp;|&nbsp; 
							<a href="">法律信息</a> &nbsp;|&nbsp; 
							<a href="">Steam 订户协议</a> &nbsp;|&nbsp; 
							<a href="">退款	</a>
						</p>
					</div>
					<a class="logimg1" href=""><img src="img/logo_steam_footer.png"></a>
				</div>
				<div class="youlian">
					<a href="">关于 Valve</a> &nbsp;| &nbsp; 
					<a href="">Steamworks</a> &nbsp;|&nbsp; 
					<a href="">工作</a> &nbsp;|&nbsp; 
					<a href="">Steam 分销</a> &nbsp;|&nbsp; 
					<a href="">礼物卡</a> &nbsp;|&nbsp; 
					<a class="facebook" href="">Steam</a> &nbsp;|&nbsp; 
					<a class="twiter" href="">@steam_games</a>
				</div>
			</div>
		</div>
	</div>
	<script src="js/best.js"></script>
	<script src="js/jquery.js"></script>
	<script src="js/index.js"></script>
	<script type="text/javascript"> 
	
	function refreshCode(){
	    var vcode = document.getElementById("vcode");
	
	    vcode.src = "${pageContext.request.contextPath}/CheckCodeServlet?time="+new Date().getTime();
	}
	function denglu(form){
		if(form.username.value==""){
			alert("用户名不能为空");
			form.uername.focus();
			return false;
		}
		if(form.password.value==""){
			alert("密码不能为空");
			form.password.focus();
			return false;
		}
	}
	setTimeout(function(){
		document.getElementById("msg").innerHTML="";},1500);
	setTimeout(function(){
		document.getElementById("message").innerHTML="";},1500);
	
	
	</script> 
</body>
</html>