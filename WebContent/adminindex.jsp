<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Steam后台管理系统</title>
	<link rel="stylesheet" type="text/css" href="css2/index.css">
</head>
<body>
	<div class="top">
		<div class="center">
			<div class="center_son topson">
				<a class="logo" href="">
					<img src="img/logo.png">
				</a>
				<div class="head_fa">
					<ul class="head_son">
						<li class="merchant_fa">
							<a class="heada" href="adminindex.html">Steam后台管理系统
							</a>
						</li>
					</ul>
				</div>
				<div class="login">
					<a class="log1" href="adminlogin.jsp">登陆</a>
				</div>
			</div>
		</div>
	</div>
	<div class="bg">
		<div class="center Fatherbox">
			<div class="center_son firstBanner">
				<div class="center_son signup">
					<div class="logL_fa">
						<div class="systemContent">
							<table class="indexTable">
								<tr>
									<td>游戏名</td>
									<td>游戏价格</td>
									<td>游戏简介</td>
									<td>操作</td>
									<td>操作</td>
									<td>操作</td>
								</tr>
								<c:forEach items="${a}" var="p" varStatus="vs">
									<tr>
									<td>
									${p.gamename}
									</td>
									<td>
									${p.price}
									</td>
									<td>
									${p.introduce}
									</td>
									<td>
									<a href="${pageContext.request.contextPath}/adminaddProduct.jsp">新增</a>
									</td>
									<td>
									<a href="${pageContext.request.contextPath}/adminDeleteServlet?gamename=${p.gamename}">删除</a>
									</td>
									<td width="80px">
									<a href="${pageContext.request.contextPath}/adminUpdateServlet?gamename=${p.gamename}">修改</a>
									</td>
									</tr>
									</c:forEach>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="tablist">
		<div class="center copyright">
			<div class="center_son bottomlog">
				<div>
					<a href="" class="logimg">
						<img src="img/logo_valve_footer.png">
					</a>
					<div class="botlogText">
						<p>©2018 Valve Corporation。保留所有权利。所有商标均为其在美国及其它国家/地区的各自持有者所有。</p>
						<p>
								所有的价格均已包含增值税（如适用）。
							<a href="">隐私政策</a> &nbsp;|&nbsp; 
							<a href="">法律信息</a> &nbsp;|&nbsp; 
							<a href="">Steam 订户协议</a> &nbsp;|&nbsp; 
							<a href="">退款	</a>
						</p>
					</div>
					<a class="logimg1" href=""><img src="img/logo_steam_footer.png"></a>
				</div>
				<div class="youlian">
					<a href="">关于 Valve</a> &nbsp;| &nbsp; 
					<a href="">Steamworks</a> &nbsp;|&nbsp; 
					<a href="">工作</a> &nbsp;|&nbsp; 
					<a href="">Steam 分销</a> &nbsp;|&nbsp; 
					<a href="">礼物卡</a> &nbsp;|&nbsp; 
					<a class="facebook" href="">Steam</a> &nbsp;|&nbsp; 
					<a class="twiter" href="">@steam_games</a>
				</div>
			</div>
		</div>
	</div>
	<script src="js/jquery.js"></script>
	<script src="js/best.js"></script>
	<script src="js/index.js"></script>
	<script src="js/data.js"></script>
	<!-- <script src="js/fuben.js"></script> -->
</body>
</html>