
<%@page import="com.steam.bean.Game"%>
<%@page import="java.sql.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<title>游戏详情页</title>
	<link rel="stylesheet" href="css/index.css">
</head>
<body>
<%try {  
    Class.forName("com.mysql.cj.jdbc.Driver");  //驱动程序名
    String url = "jdbc:mysql://localhost:3306/Steam"; //数据库名
    String username = "root";  //数据库用户名
    String password = "Aa123456";  //数据库用户密码
    Connection conn = DriverManager.getConnection(url, username, password);  //连接状态

%>

	<div class="top gaintro_top">
		<div class="center_son topson  gaintro_topson">
			<a class="logo" href="">
				<img src="img/logo.png">
			</a>
			<div class="head_fa">
				<ul class="head_son">
					<li class="merchant_fa">
						<a class="heada" href="index.jsp">商店
						</a>
					</li>
					<li class="community_fa">
						<a class="heada" href="shoppingcar.jsp">购物车
						</a>
					</li>
					<li>
						<a class="heada" href="">关于</a>
					</li>
					<li>
						<a class="heada" href="">客服</a>
					</li>
				</ul>
			</div>
			<div class="login">
					<!-- <a class="log1" href="login.html">登陆</a> -->
					<c:if test="${not empty user }">
						<a href="${pageContext.request.contextPath }/LoginoutServlet"><h6 style="color: white;">欢迎你回来${sessionScope.user.username }!</h6>注销</a>
					<span>|</span>
						<a href="${pageContext.request.contextPath }/signup.jsp">注册</a>
					</c:if>
					
					<c:if test="${empty user }">
						<a href="${pageContext.request.contextPath }/loginjsp.jsp">登录</a>
						<span>|</span>
						<a href="${pageContext.request.contextPath }/signup.jsp">注册</a>
					</c:if>
					<!-- <a class="log1" href="signup.html">注册</a> -->
				</div>
		</div>
	</div>
	<%
                Statement stmt = null;  
                ResultSet rs = null;  
                String sql = "SELECT * FROM game;";  //查询语句
                stmt = conn.createStatement();  
                rs = stmt.executeQuery(sql);  
                while (rs.next()) {%>
	<div class="bg gameintro">
		<div class="center Fatherbox gaintro_father">
			<div class="center_son shequ_fa">
			</div>
			<div class="center_son gameVideo">
				<div class="gameVleft">
					<div class="Video" >
						<img id="fatherImg" src="img/6.jpg">
					</div>
				</div>
				<div class="gameVright">
					<div>
						<img src="img/6.jpg">
					</div>
					
					<div class="civtext">
					<%=rs.getString("gameBrief") %>
					</div>
					<div class="buy">
						<div class="tianjia">
							<span><%=rs.getString("gamePrice") %></span>
							<a class="green tianjia1" href="${pageContext.request.contextPath }/GameServlet">添加至购物车</a>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
 <%
            }  
            
        }catch (Exception e) {        
            //e.printStackTrace();  
            out.print("数据库连接异常！");  
        }  
%> 
	<div class="tablist">
		<div class="center copyright gaintro_bot">
			<div class="center_son bottomlog gintro_bottom">
				<div>
					<a href="" class="logimg">
						<img src="img/logo_valve_footer.png">
					</a>
					<div class="botlogText">
						<p>©2018 Valve Corporation。保留所有权利。所有商标均为其在美国及其它国家/地区的各自持有者所有。</p>
						<p>
								所有的价格均已包含增值税（如适用）。
							<a href="">隐私政策</a> &nbsp;|&nbsp; 
							<a href="">法律信息</a> &nbsp;|&nbsp; 
							<a href="">Steam 订户协议</a> &nbsp;|&nbsp; 
							<a href="">退款	</a>
						</p>
					</div>
					<a class="logimg1" href=""><img src="img/logo_steam_footer.png"></a>
				</div>
				<div class="youlian">
					<a href="">关于 Valve</a> &nbsp;| &nbsp; 
					<a href="">Steamworks</a> &nbsp;|&nbsp; 
					<a href="">工作</a> &nbsp;|&nbsp; 
					<a href="">Steam 分销</a> &nbsp;|&nbsp; 
					<a href="">礼物卡</a> &nbsp;|&nbsp; 
					<a class="facebook" href="">Steam</a> &nbsp;|&nbsp; 
					<a class="twiter" href="">@steam_games</a>
				</div>
			</div>
		</div>
	</div>
	<script src="js/jquery.js"></script>
	<script src="js/index.js"></script>

</body>
</html>