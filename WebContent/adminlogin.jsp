<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>登陆</title>
	<link rel="stylesheet" href="css2/index.css">
</head>
<body>
	<div class="top gaintro_top">
		<div class="center_son topson  gaintro_topson">
			<a class="logo" href="">
				<img src="img/logo.png">
			</a>
			<div class="head_fa">
				<ul class="head_son">
					<li class="merchant_fa">
						<a class="heada" href="adminindex.jsp">Steam后台管理系统
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="bg">
		<div class="center Fatherbox gaintro_father">
			<div class="center_son logInAdmin">
				<div class="logL_fa">
					<form name="form11" action="adminloginCheckServlet" method="post">
					<div class="logLeft">
						<div class="logLeft_son1">
							<h2>管理员账户登陆</h2>
							
							<div class="logLeft_son">
								<p class="logLp">Steam 管理员账户名</p>
								<div>
									<input name="username" type="text" id="username">
								</div>
							</div>
							<div class="logLeft_son">
								<p class="logLp">Steam 管理员密码</p>
								<div>
									<input type="password" name="password" id="pass">
								</div>
							</div>
							
							
						</div>

						<div class="btn_fa">
							<div class="logBtnL">
								<button class="logBtn">登陆</button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="tablist">
		<div class="center copyright gaintro_bot">
			<div class="center_son bottomlog gintro_bottom">
				<div>
					<a href="" class="logimg">
						<img src="img/logo_valve_footer.png">
					</a>
					<div class="botlogText">
						<p>©2018 Valve Corporation。保留所有权利。所有商标均为其在美国及其它国家/地区的各自持有者所有。</p>
						<p>
								所有的价格均已包含增值税（如适用）。
							<a href="">隐私政策</a> &nbsp;|&nbsp; 
							<a href="">法律信息</a> &nbsp;|&nbsp; 
							<a href="">Steam 订户协议</a> &nbsp;|&nbsp; 
							<a href="">退款	</a>
						</p>
					</div>
					<a class="logimg1" href=""><img src="img/logo_steam_footer.png"></a>
				</div>
				<div class="youlian">
					<a href="">关于 Valve</a> &nbsp;| &nbsp; 
					<a href="">Steamworks</a> &nbsp;|&nbsp; 
					<a href="">工作</a> &nbsp;|&nbsp; 
					<a href="">Steam 分销</a> &nbsp;|&nbsp; 
					<a href="">礼物卡</a> &nbsp;|&nbsp; 
					<a class="facebook" href="">Steam</a> &nbsp;|&nbsp; 
					<a class="twiter" href="">@steam_games</a>
				</div>
			</div>
		</div>
	</div>
	<script src="js/best.js"></script>
	<script src="js/jquery.js"></script>
	<script src="js/index.js"></script>
</body>
</html>