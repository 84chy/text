<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>steam</title>
<link rel="stylesheet" type="text/css" href="css/index.css">
</head>
<body>

	<div class="top">
		<div class="center">
			<div class="center_son topson">
				<a class="logo" href=""> <img src="img/logo.png">
				</a>
				<div class="head_fa">
					<ul class="head_son">
						<li class="merchant_fa"><a class="heada" href="index.jsp">商店
						</a></li>
						<li class="community_fa"><a class="heada" href="shoppingcar.jsp">购物车 </a></li>
						<li><a class="heada" href="">关于</a></li>
						<li><a class="heada" href="">客服</a></li>
					</ul>
				</div>
				<div class="login">
					<!-- <a class="log1" href="login.html">登陆</a> -->
					<c:if test="${not empty user }">
						<a href="${pageContext.request.contextPath }/LoginoutServlet"><h6 style="color: white;">欢迎你回来${sessionScope.user.username }!</h6>注销</a>
					<span>|</span>
						<a href="${pageContext.request.contextPath }/signup.jsp">注册</a>
					</c:if>
					
					<c:if test="${empty user }">
						<a href="${pageContext.request.contextPath }/loginjsp.jsp">登录</a>
						<span>|</span>
						<a href="${pageContext.request.contextPath }/signup.jsp">注册</a>
					</c:if>
					<!-- <a class="log1" href="signup.html">注册</a> -->
				</div>
			</div>
		</div>
	</div>
	<div class="bg">
		<div class="center Fatherbox">
			<div class="center_son firstBanner">
				<h2>精选和推荐</h2>
				<div class="bannerFa  banFaShow">
					<div class="bannerSon_left">
						<a href="gameintro.html"> <img src="img/cyberpunk2077.jpg"
							alt="photo">
						</a>
					</div>
					<div class="bannerSon_right">

						<div class="bannerSon_text">
							<p class="tuichu">cyberpunk2077</p>
						</div>
						<div class="img_fa">

							<div class="banner1Img">
								<img src="img/cyberpunk20772.jpg" alt="photo"> <a
									class="litterimg" href=> <a href="gameintro.jsp"
									class="cover"></a>
								</a>
							</div>

							<div class="banner1Img img4">
								<img src="img/cyberpunk20773.jpg" alt="photo"> <a
									class="litterimg" href=""> <a href="gameintro.jsp"
									class="cover"></a>
								</a>
							</div>
						</div>
						<p class="tuichu">现已推出</p>
						<div class="tianjiaindex">
							<span>￥298</span> <a class="green tianjia1index" href="">添加至购物车</a>
						</div>
						<div class="money-fa">
							<!-- <div class="money gray">
								<span class="gray_green"></span>
								<span class="lineThrough orginmoney"></span>
								<span class="moneyl"></span>
							</div> -->
							<div class="system-icon"></div>
						</div>
					</div>

				</div>
				<!-- <div class="clickLeft FirLeft">
					<div class="clickLson">
					</div>
				</div>
				<div class="clickRight FirRight">
					<div class="clickRson">
					</div>
				</div> -->
			</div>
			<div class="circle center_son FirstCircle"></div>
			<div class="circle center_son SecCircle"></div>
			<div class="circle center_son ThiCircle"></div>
		</div>
		<div class="center gameTab">
			<div class="center_son gameList show">
				<div class="gameLeft">
					<div class="game1" name="game1">
						<a href=""><img src="img/game1.jpg"></a>
						<div class="gameText">
							<p class="gamep1">PLAYERUNKNOWN'S BATTLEGROUNDS</p>
							<div class="gamediv1">
								<span></span>
							</div>
							<p class="gamep2">动作，开放世界，砍杀，玩家对战</p>
							<a class="gameTexta" href="gameintro.jsp"></a>
							<div class="tianjiaindex">
							<span>￥99</span>
								<a href="shoppingcar.jsp"><input type="submit" name="submit" class="green tianjia1index" value="添加至购物车"></a>
							</div>
						</div>
						<!-- <div class="gameText_money"></div> -->
					</div>
					<div class="game1" name="game2">
						<a href=""><img src="img/game2.jpg"></a>
						<div class="gameText">
							<p class="gamep1">DYNASTY WARRIORS 9:Season
								Pass/真・三國無双８:シーズンパス</p>
							<div class="gamediv1">
								<span></span>
							</div>
							<p class="gamep2">动作，开放世界，砍杀，玩家对战</p>
							<a class="gameTexta" href="gameintro.jsp"></a>
							<div class="tianjiaindex">
								<span>￥99</span> 
								<button type="submit" class="green tianjia1index" >添加至购物车</button>
							</div>
						</div>
					</div>
					<div class="game1" name="game3">
						<a href=""><img src="img/game3.jpg"></a>
						<div class="gameText">
							<p class="gamep1">PLAYERUNKNOWN'S BATTLEGROUNDS</p>
							<div class="gamediv1">
								<span></span>
							</div>
							<p class="gamep2">动作，开放世界，砍杀，玩家对战</p>
							<a class="gameTexta" href="gameintro.jsp"></a>
							<div class="tianjiaindex">
								<span>￥99</span> <a class="green tianjia1index" href="">添加至购物车</a>
							</div>
						</div>
					</div>
					<div class="game1">
						<a href=""><img src="img/game4.jpg"></a>
						<div class="gameText">
							<p class="gamep1">PLAYERUNKNOWN'S BATTLEGROUNDS</p>
							<div class="gamediv1">
								<span></span>
							</div>
							<p class="gamep2">动作，开放世界，砍杀，玩家对战</p>
							<a class="gameTexta" href="gameintro.jsp"></a>
							<div class="tianjiaindex">
								<span>￥99</span> <a class="green tianjia1index" href="">添加至购物车</a>
							</div>
						</div>
					</div>
					<div class="game1">
						<a href=""><img src="img/game5.jpg"></a>
						<div class="gameText">
							<p class="gamep1">PLAYERUNKNOWN'S BATTLEGROUNDS</p>
							<div class="gamediv1">
								<span></span>
							</div>
							<p class="gamep2">动作，开放世界，砍杀，玩家对战</p>
							<a class="gameTexta" href="gameintro.jsp"></a>
							<div class="tianjiaindex">
								<span>￥99</span> <a class="green tianjia1index" href="">添加至购物车</a>
							</div>
						</div>
					</div>
					<div class="game1">
						<a href=""><img src="img/game6.jpg"></a>
						<div class="gameText">
							<p class="gamep1">PLAYERUNKNOWN'S BATTLEGROUNDS</p>
							<div class="gamediv1">
								<span></span>
							</div>
							<p class="gamep2">动作，开放世界，砍杀，玩家对战</p>
							<a class="gameTexta" href="gameintro.jsp"></a>
							<div class="tianjiaindex">
								<span>￥99</span> <a class="green tianjia1index" href="">添加至购物车</a>
							</div>
						</div>
					</div>
					<div class="game1">
						<a href=""><img src="img/game7.jpg"></a>
						<div class="gameText">
							<p class="gamep1">PLAYERUNKNOWN'S BATTLEGROUNDS</p>
							<div class="gamediv1">
								<span></span>
							</div>
							<p class="gamep2">动作，开放世界，砍杀，玩家对战</p>
							<a class="gameTexta" href="gameintro.jsp"></a>
							<div class="tianjiaindex">
								<span>￥99</span> <a class="green tianjia1index" href="">添加至购物车</a>
							</div>
						</div>
					</div>
					<div class="game1">
						<a href=""><img src="img/game8.jpg"></a>
						<div class="gameText">
							<p class="gamep1">PLAYERUNKNOWN'S BATTLEGROUNDS</p>
							<div class="gamediv1">
								<span></span>
							</div>
							<p class="gamep2">动作，开放世界，砍杀，玩家对战</p>
							<a class="gameTexta" href="gameintro.jsp"></a>
							<div class="tianjiaindex">
								<span>￥99</span> <a class="green tianjia1index" href="">添加至购物车</a>
							</div>
						</div>
					</div>
					<div class="game1">
						<a href=""><img src="img/game9.jpg"></a>
						<div class="gameText">
							<p class="gamep1">PLAYERUNKNOWN'S BATTLEGROUNDS</p>
							<div class="gamediv1">
								<span></span>
							</div>
							<p class="gamep2">动作，开放世界，砍杀，玩家对战</p>
							<a class="gameTexta" href="gameintro.jsp"></a>
							<div class="tianjiaindex">
								<span>￥99</span> <a class="green tianjia1index" href="">添加至购物车</a>
							</div>
						</div>
					</div>
					<div class="game1">
						<a href=""><img src="img/game10.jpg"></a>
						<div class="gameText">
							<p class="gamep1">PLAYERUNKNOWN'S BATTLEGROUNDS</p>
							<div class="gamediv1">
								<span></span>
							</div>
							<p class="gamep2">动作，开放世界，砍杀，玩家对战</p>
							<a class="gameTexta" href="gameintro.jsp"></a>
							<div class="tianjiaindex">
								<span>￥99</span> <a class="green tianjia1index" href="">添加至购物车</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<div class="tablist">
		<div class="center copyright">
			<div class="center_son bottomlog">
				<div>
					<a href="" class="logimg"> <img src="img/logo_valve_footer.png">
					</a>
					<div class="botlogText">
						<p>©2018 Valve Corporation。保留所有权利。所有商标均为其在美国及其它国家/地区的各自持有者所有。</p>
						<p>
							所有的价格均已包含增值税（如适用）。 <a href="">隐私政策</a> &nbsp;|&nbsp; <a href="">法律信息</a>
							&nbsp;|&nbsp; <a href="">Steam 订户协议</a> &nbsp;|&nbsp; <a href="">退款
							</a>
						</p>
					</div>
					<a class="logimg1" href=""><img src="img/logo_steam_footer.png"></a>
				</div>
				<div class="youlian">
					<a href="">关于 Valve</a> &nbsp;| &nbsp; <a href="">Steamworks</a>
					&nbsp;|&nbsp; <a href="">工作</a> &nbsp;|&nbsp; <a href="">Steam
						分销</a> &nbsp;|&nbsp; <a href="">礼物卡</a> &nbsp;|&nbsp; <a
						class="facebook" href="">Steam</a> &nbsp;|&nbsp; <a class="twiter"
						href="">@steam_games</a>
				</div>
			</div>
		</div>
	</div>
	<script src="js/jquery.js"></script>
	<script src="js/best.js"></script>
	<script src="js/index.js"></script>
	<script src="js/data.js"></script>
	<!-- <script src="js/fuben.js"></script> -->
</body>
</html>
