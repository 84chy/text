package com.steam.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.swing.JOptionPane;

import com.mchange.v2.c3p0.ComboPooledDataSource;


/**
 * Servlet implementation class ValidationServlet
 */
@WebServlet(name="validationServlet",urlPatterns= {"/validation.do"})
public class ValidationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ValidationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    Connection dbconn=null;
    DataSource dataSource = null;
    	    //初始化C3P0数据源
    public void init() {
    	try {
    		
            ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource("hhh");
    	    dataSource = comboPooledDataSource;
    	    dbconn=dataSource.getConnection();
    	}catch(Exception e) {
    		}
    	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/xml;charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		String message="该用户名可以被使用";
		String username=request.getParameter("username");
		String sql="SELECT * FROM users WHERE username=?";
		try {
			PreparedStatement pstmt=dbconn.prepareStatement(sql);
			pstmt.setString(1, username);
			ResultSet rst=pstmt.executeQuery();
			if(rst.next())
				message="该用户名已被使用，请更换其他用户名！";
		}catch(Exception sqle) {
			System.out.println(sqle);
		}
		PrintWriter out=response.getWriter();
		out.println("<response>");
		out.println("<message>"+message+"</message>");
		out.println("</response>");
		}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=utf-8");
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		String verifycode=request.getParameter("verifycode");
		try {
			if (verifycode=="") {
				String login_message="请输入验证码！";
				request.getSession().setAttribute("login_message",login_message);
				response.sendRedirect("signup.jsp");
			}else {
				String sql="INSERT INTO users VALUES(?,?)";
				PreparedStatement pstmt=dbconn.prepareStatement(sql);
				pstmt.setString(1, username);
				pstmt.setString(2, password);
				if(pstmt.executeUpdate()>0)
				 {
					JOptionPane.showMessageDialog(null, "注册成功！");
		request.getRequestDispatcher("transition.jsp").forward(request,response);}
			}			
		}catch(Exception e) {
			System.out.println(e);
		}
	}

}
