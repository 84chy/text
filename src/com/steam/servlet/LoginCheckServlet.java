package com.steam.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.eclipse.jdt.internal.compiler.impl.Constant;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.steam.bean.User;




/**
 * Servlet implementation class LoginCheckServlet
 */
@WebServlet("/LoginCheckServlet")
public class LoginCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    String message=null;  
    boolean flag=false;
    Connection dbconn=null;
    DataSource dataSource = null;
    public void init() {
    	try {
    		  ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource("hhh");
    	        dataSource = comboPooledDataSource;
    	        dbconn=dataSource.getConnection();
    	}catch(Exception e) {}
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");	
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		User user=new User();
		user.setUsername(username);
		user.setPassword(password);
		request.getSession().setAttribute("user", user);
		String value1="";
		String value2="";
		String value3="";
		Cookie cookie=null;
		Cookie [] cookies=request.getCookies();
		if(cookies!=null) {
			for(int i=0;i<cookies.length;i++) {
				cookie=cookies[i];
				if(cookie.getName().equals("username"))
					value1=cookie.getValue();
				if(cookie.getName().equals("password"))
					value2=cookie.getValue();
				if(cookie.getName().equals("verifycode"))
					value3=cookie.getValue();
			}
			if(flag) {
				response.sendRedirect("loginjsp.jsp?username="+value1.toString()+"再次登录");
		}else {
			response.sendRedirect("loginjsp.jsp");
		}
	}else {
		response.sendRedirect("loginjsp.jsp");
	}
}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		String username=request.getParameter("username").trim();//trim()去掉用户名字符串的头尾两端空格
		String password=request.getParameter("password").trim();
		String verifycode=request.getParameter("verifycode").trim();
		try {
			String sql="select * from users where username = ? and password = ?";
			PreparedStatement pstmt=dbconn.prepareStatement(sql);
			pstmt.setString(1,request.getParameter("username"));
			pstmt.setString(2,request.getParameter("password"));
			ResultSet result=pstmt.executeQuery();
			
			while(result.next()) {
			flag=true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}	
		
		
		if(!flag||verifycode==""){
			message="用户名或密码不正确，请重试！";
			request.getSession().setAttribute("message",message);
			String login_message="请输入验证码！";
			request.getSession().setAttribute("login_message",login_message);
			response.sendRedirect("loginjsp.jsp");
		}else {
			User user=new User();
			user.setUsername(username);
			user.setPassword(password);
			request.getSession().setAttribute("user", user);
			if((request.getParameter("check")!=null&&request.getParameter("check").equals("check"))) {//选了记住密码
				Cookie usernameCookie=new Cookie("username",username);
				Cookie passwordCookie=new Cookie("password",password);
				usernameCookie.setMaxAge(60*60*4);
				passwordCookie.setMaxAge(60*60*4);
				response.addCookie(usernameCookie);
				response.addCookie(passwordCookie);
			}
			response.sendRedirect("index.jsp?username="+username.toString());
		}	
	}
}
