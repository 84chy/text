package com.steam.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.steam.bean.User;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter(filterName = "LoginFilter",urlPatterns = "/*")
public class LoginFilter implements Filter {

    /**
     * Default constructor. 
     */
    public LoginFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request2=(HttpServletRequest) request;
		HttpServletResponse response2=(HttpServletResponse) response;
		Cookie[] cookies=request2.getCookies();
		String autologin=null;
		for(int i=0;cookies!=null&&i<cookies.length;i++) {
			if ("check".equals(cookies[i].getName())) {
				autologin=cookies[i].getValue();
				break;
			}
		}
		if (autologin!=null) {
			String[] parts=autologin.split("-");
			String username=parts[0];
			String password=parts[1];
			if (username!=""&&password!="") {
				User user=new User();
				user.setUsername(username);
				user.setPassword(username);
				request2.getSession().setAttribute("user",user);
			}
		
		}
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		if (username==""&&password=="") {
			User user=new User();
			user.setUsername(username);
			user.setPassword(username);
			response2.sendRedirect("loginjsp.jsp");
		}
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
