package com.steam.bean;

import java.io.Serializable;

public class Game implements Serializable{
	
	private  String gameName;
	private  String gamePrice;
	private  String gameBrief;
	private  String gamePhoto;
	
//	
//	public Game(String gameName, String gamePrice, String gameBrief, String gamePhoto) {
//		super();
//		this.gameName = gameName;
//		this.gamePrice = gamePrice;
//		this.gameBrief = gameBrief;
//		this.gamePhoto = gamePhoto;
//	}
//	@Override
//	public String toString() {
//		return "Game [gameName=" + gameName + ", gamePrice=" + gamePrice + ", gameBrief=" + gameBrief + ", gamePhoto="
//				+ gamePhoto + "]";
//	}
	public String getGameName() {
		return gameName;
	}
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	public String getGamePrice() {
		return gamePrice;
	}
	public void setGamePrice(String gamePrice) {
		this.gamePrice = gamePrice;
	}
	public String getGameBrief() {
		return gameBrief;
	}
	public void setGameBrief(String gameBrief) {
		this.gameBrief = gameBrief;
	}
	public String getGamePhoto() {
		return gamePhoto;
	}
	public void setGamePhoto(String gamePhoto) {
		this.gamePhoto = gamePhoto;
	}
	

	
	
	

	
}
