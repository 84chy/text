package com.admin.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class adminUpdateSecondServlet
 */
@WebServlet("/adminUpdateSecondServlet")
public class adminUpdateSecondServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public adminUpdateSecondServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		request.setCharacterEncoding("UTF-8");
		
		
		String gamename = request.getParameter("gamename");		
		String price = request.getParameter("price");		
		String introduce = request.getParameter("introduce");	
		
		String sql = "update games set price=?,introduce=? where gamename=?";
		
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
		
			String url = "jdbc:mysql://localhost:3306/steam?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC&useSSL=true";			
			String user = "root";			
			String pass = "a123456";
			
			Connection conn = DriverManager.getConnection(url,user,pass);		    
		    PreparedStatement ps = conn.prepareStatement(sql);		    
		    ps.setString(1, price);		    
		    ps.setString(2, introduce);		    	    		    
		    ps.setString(3, gamename);		    
//		    ps.setInt(5, Integer.parseInt(stuNum));		    
		    ps.executeUpdate(); 
		    response.sendRedirect("adminGamesListServlet");
		    
		    ps.close();
		    
		    conn.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
