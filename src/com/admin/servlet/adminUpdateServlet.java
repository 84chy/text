package com.admin.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.steam.bean.GamesItem;


/**
 * Servlet implementation class adminUpdateServlet
 */
@WebServlet("/adminUpdateServlet")
public class adminUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public adminUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("UTF-8");
		String gamename = request.getParameter("gamename");
		
		try {
			
			Class.forName("com.mysql.cj.jdbc.Driver");
		
			String url = "jdbc:mysql://localhost:3306/steam?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC&useSSL=true";			
			String user = "root";			
			String pass = "a123456";
			
			Connection conn = DriverManager.getConnection(url,user,pass);
			
			String sql = "select * from games where gamename=?";
			
			PreparedStatement ps  = conn.prepareStatement(sql);			
			ps.setString(1, gamename);
			
			ResultSet rs = ps.executeQuery();			
			rs.next();			
			GamesItem p = new GamesItem();			
			p.setGamename(rs.getNString("gamename"));			
			p.setPrice(rs.getDouble("price"));			
			p.setIntroduce(rs.getNString("introduce"));
			
			request.setAttribute("m", p);

			request.getRequestDispatcher("adminchangeProduct.jsp").forward(request, response);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
