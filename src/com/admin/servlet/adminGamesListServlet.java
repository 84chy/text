package com.admin.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.steam.bean.User;
import com.steam.bean.GamesItem;

/**
 * Servlet implementation class GamesListServlet
 */
@WebServlet("/adminGamesListServlet")
public class adminGamesListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
//	static final String DB_URL="jdbc:mysql://localhost:3306/steam?serverTimezone=UTC";
//	static final String USER="root";
//	static final String PASS="a123456";   
   
    public adminGamesListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 try { 
				
				Class.forName("com.mysql.cj.jdbc.Driver");    //com.mysql.jdbc.Driver
				
				String url = "jdbc:mysql://localhost:3306/steam?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC&useSSL=true";
			
				String user = "root";
				
				String pass = "a123456";
				
				Connection conn = DriverManager.getConnection(url,user,pass);
			
	            String sql = "select * from games";
				
				PreparedStatement ps = conn.prepareStatement(sql);
				
				ResultSet rs = ps.executeQuery();
				
				ArrayList<GamesItem> list = new ArrayList<GamesItem>();
				
				while(rs.next()) {
					GamesItem record=new GamesItem();//实例化对象
					record.setGid(rs.getInt("gid"));
					record.setGamename(rs.getNString("gamename"));
					record.setPrice(rs.getDouble("price"));
					record.setImg(rs.getString("img"));
					record.setIntroduce(rs.getNString("introduce"));
					list.add(record);//丢到list集合里
					
				}
				
	            request.setAttribute("a", list);
				
				request.getRequestDispatcher("adminindex.jsp").forward(request, response);
				
				rs.close();
				
				ps.close();
				
				conn.close();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("无法连接！！");
			}
		
		
		 

	}
    
	

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
