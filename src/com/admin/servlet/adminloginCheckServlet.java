package com.admin.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.steam.bean.User;
/**
 * Servlet implementation class adminloginCheckServlet
 */
@WebServlet("/adminloginCheckServlet")
public class adminloginCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String message=null;  
	Connection dbconn=null;
    DataSource dataSource = null;
    boolean flag=false;
    public void init() {
    	try {
    		  ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource("hhh");
    	        dataSource = comboPooledDataSource;
    	        dbconn=dataSource.getConnection();
    	}catch(Exception e) {}
    }
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public adminloginCheckServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html,utf-8");
		String value1="";
		String value2="";
		Cookie cookie=null;
		Cookie [] cookies=request.getCookies();
		if(cookies!=null) {
			for(int i=0;i<cookies.length;i++) {
				cookie=cookies[i];
				if(cookie.getName().equals("username"))
					value1=cookie.getValue();
				if(cookie.getName().equals("password"))
					value2=cookie.getValue();
			}
			if(flag) {
				response.sendRedirect("adminloginjsp.jsp?username="+value1.toString()+"再次登录");
		}else {
			response.sendRedirect("adminloginjsp.jsp");
		}
	}else {
		response.sendRedirect("adminloginjsp.jsp");
	}
		
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=utf-8");
		String username=request.getParameter("username").trim();//trim()去掉用户名字符串的头尾两端空格
		String password=request.getParameter("password").trim();
		try {
			String sql="select * from admin where username = ? and password = ?";
			PreparedStatement pstmt=dbconn.prepareStatement(sql);
			pstmt.setString(1,request.getParameter("username"));
			pstmt.setString(2,request.getParameter("password"));
			ResultSet result=pstmt.executeQuery();
			
			while(result.next()) {
			flag=true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}	
		if(!flag){
			message="用户名或密码不正确，请重试！";
			request.getSession().setAttribute("message",message);
			response.sendRedirect("adminlogin.jsp");
		}else {
			User user=new User();
			user.setUsername(username);
			user.setPassword(password);
			request.getSession().setAttribute("admin", user);
			response.sendRedirect("adminGamesListServlet");
	}

	}
}
