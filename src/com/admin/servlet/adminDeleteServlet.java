package com.admin.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class adminDeleteServlet
 */
@WebServlet("/adminDeleteServlet")
public class adminDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public adminDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String gamename = request.getParameter("gamename");
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");		
            String url = "jdbc:mysql://localhost:3306/steam?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC&useSSL=true";			
			String user = "root";			
			String pass = "a123456";			
			Connection conn = DriverManager.getConnection(url,user,pass);			
			String sql = "delete from games where gamename=?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, gamename);	
			ps.executeUpdate();			
			response.sendRedirect("adminGamesListServlet");	
			
			ps.close();		
			conn.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
