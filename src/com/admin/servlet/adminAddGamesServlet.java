package com.admin.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddGamesServlet
 */
@WebServlet("/adminAddGamesServlet")
public class adminAddGamesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L; 
    
    public adminAddGamesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		
		String gamename = request.getParameter("gamename");
		String price = request.getParameter("price");
		String introduce = request.getParameter("introduce");
		
        try {
			
        	Class.forName("com.mysql.cj.jdbc.Driver");    //com.mysql.jdbc.Driver
			String url = "jdbc:mysql://localhost:3306/steam?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC&useSSL=true";
			String user = "root";
			String pass = "a123456";
			Connection conn = DriverManager.getConnection(url,user,pass);	
			
			
			String sql = "insert into games(gamename,price,introduce) values(?,?,?)";
			
			PreparedStatement ps  = conn.prepareStatement(sql);
		    ps.setString(1, gamename);	    
            ps.setString(2, price);
		    ps.setString(3, introduce);
		    
			int result = ps.executeUpdate();
			
			response.sendRedirect("adminGamesListServlet");
			System.out.println("成功插入！");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("失败！");
		}
		
		
		
//		response.setContentType("text/html;charset=utf-8");
//		
//		Integer gid = Integer.valueOf(request.getParameter("gid"));
//		String gamename = request.getParameter("gamename");
//		String nameString=new String(gamename.getBytes("ISO8859-1"),"UTF-8");
//		Integer price = Integer.valueOf(request.getParameter("price"));
//		String img = request.getParameter("img");
//		
//		Connection conn = null;
//		Statement stmt = null;
//		try {
//			Class.forName("com.mysql.cj.jdbc.Driver");
//		}
//
//		catch (ClassNotFoundException e) {
//			System.out.println("连接失败!");
//		}
//		try {
//			String url = "jdbc:mysql://localhost:3306/steam?serverTimezone=UTC";
//			conn = DriverManager.getConnection(url, "root", "a123456");
////            Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);
//            //操作数据库
//            String sql = "insert into games(gid,gamename,price,img) values(?,?,?,?)";
//			PreparedStatement ps = conn.prepareStatement(sql); 
//			ps.setInt(1, gid); 
//		    ps.setString(2, nameString); 
//		    ps.setInt(3, price); 
//		    ps.setString(4, img); 
//		    int row = ps.executeUpdate(); 		     
//			if (row > 0) {
//				request.getRequestDispatcher("/adminGamesListServlet").forward(request, response);
//				System.out.println("添加成功!");
//			} else {
//				System.out.println("添加失败!");
//			}
//
//		} catch (Exception e) {
//			System.out.println("连接数据库失败!");
//		} finally {
//
//			if (stmt != null) {
//				try {
//					stmt.close();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
//				stmt = null;
//			}
//			if (conn != null) {
//				try {
//					conn.close();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
//				conn = null;
//			}
//		}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
