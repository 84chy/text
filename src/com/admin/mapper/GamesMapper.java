package com.admin.mapper;

import java.util.List;

import com.steam.bean.GamesItem;

public interface GamesMapper {
	
	int delete(int gid);
	boolean insert(GamesItem record);
	List<GamesItem> findByCname(String cname);
	List<GamesItem> selectAll();
	
}
